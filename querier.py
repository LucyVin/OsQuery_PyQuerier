import subprocess
import json
from io import StringIO

class Query:
    def __init__(self, table, order=None, orderDir=None, limit=None):
        self.table = table
        self.limit = limit
        self.order = order
        self.orderDir = orderDir

    def __queryConstructor(self):
        base = 'select * from %s' % self.table
        if self.order:
            base += ' order by %s' % self.order
            if self.orderDir:
                base += ' ' + self.orderDir
        if self.limit:
            base += ' limit %s' % str(self.limit)
        return base

    def selectAll(self):
        q = subprocess.Popen(('osqueryi', '--json', '-A', self.table), stdout=subprocess.PIPE)
        o = str(q.stdout.read(),'utf-8')
        j = json.load(StringIO(o))
        return j

    def select(self):
        q = subprocess.Popen(('osqueryi', '--json', self.__queryConstructor()), stdout=subprocess.PIPE)
        o = str(q.stdout.read(), 'utf-8')
        j = json.load(StringIO(o))
        return j

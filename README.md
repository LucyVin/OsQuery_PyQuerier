# OSQuery Python Query Handler

A small project intended to allow handling of basic OSQuery interaction from Python. It is bad. It should not be used in production. It is probably insanely insecure. You've been warned.

## Usage

Using the Query class is as simple as instantiating a class:

```python3
>>> from querier import Query
>>> q = Query('processes')
>>> q.selectAll()
```

The `Query` class can be initialized with the following:

+ Table (required) - table you want to query
+ Order - the Order By column 
+ OrderDir - should be 'Asc' or 'Desc'
+ Limit - limit value

The Query class has two methods: `select()` and `selectAll()`

`select()` will apply limits and orders, and should be used when that sort of filtering is necessary. `selectAll()` passes the -A flag to osqueryi and will not do any filtering.

